#include "server.h"

#include <iostream>
#include <thread>
#include <string>
#include <boost/bind.hpp>

Server::Server(io_service &ios) :
    _ios(ios),
    acc(std::make_unique<ip::tcp::acceptor>(ios, ip::tcp::v4(), 25000))
{
    std::cout<<__FUNCTION__<<"\n";
    startAccept();
}

Server::~Server()
{
    std::cout<<__FUNCTION__<<"\n";
}

void Server::startAccept()
{
    std::cout<<__FUNCTION__<<"\n";

    for(;;)
    {
        ip::tcp::socket sock(_ios);
        acc->async_accept(sock, boost::bind(&Server::handleAccept, this, _1));
    }
}

void Server::handleAccept(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Error in accept: "<<err<<"\n";
        doClose();
    }
    doServerRead();
}

void Server::doClose()
{
    std::cout<<__FUNCTION__<<"\n";
    if(sock.is_open())
    {
        sock.close();
    }
    if(acc && acc->is_open())
    {
        acc->close();
    }
    throw std::logic_error("timeout");
}

//Ожидание чтения заголовка пакета
void Server::doServerRead()
{
    std::cout<<__FUNCTION__<<"\n";
    try
    {
        async_read(sock, buffer(headerPackage), boost::asio::transfer_exactly(10), boost::bind(&Server::handleHeaderRead, this, _1));
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what();
    }
}

//Обработка прочитанного заголовка пакета
void Server::handleHeaderRead(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Error read: "<<err<<"\n";
        doClose();
    }

    std::cout<<"Message from client. Header: "<<headerPackage.data()<<"\n";

    std::stringstream str(std::move(std::string(headerPackage.begin(), headerPackage.end())));
    size_t lenBody = std::move(std::stoi(str.str()));
    bodyPackage = std::make_unique<char[]>(lenBody);
    async_read(sock, buffer(bodyPackage.get(), lenBody), boost::asio::transfer_at_least(lenHeader+1), boost::bind(&Server::handleBodyRead, this, _1));
}

void Server::handleBodyRead(const boost::system::error_code &err)
{
    std::cout<<"Message from client. Body: "<<bodyPackage.get()<<"\n";
   // doWriteAnswer();
}

void Server::doWriteAnswer()
{
    std::cout<<__FUNCTION__<<"\n";
   // std::string message1 = make_daytime_string();
    //async_write(sock, buffer(message), boost::bind(&Server::handleWriteAnswer, this, _1));
}

void Server::handleWriteAnswer(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Error write answer: "<<err<<"\n";
        doClose();
    }
    doServerRead();
}

