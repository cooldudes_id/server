#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>

using namespace boost::asio;

class Server : private boost::noncopyable
{
public:
    Server(io_service &ios);
    ~Server();

private:
    constexpr static size_t lenHeader{10};
    std::array<char, 10> headerPackage;
    std::unique_ptr<char[]> bodyPackage{nullptr};
    io_service &_ios;

    std::unique_ptr<ip::tcp::acceptor> acc{nullptr};

    void startAccept();
    void handleAccept(const boost::system::error_code &err);
    void doClose();
    void doServerRead();
    void handleHeaderRead(const boost::system::error_code &err);
    void handleBodyRead(const boost::system::error_code &err);
    void doWriteAnswer();
    void handleWriteAnswer(const boost::system::error_code &err);
    void runService(std::shared_ptr<io_service> ios_ptr);
};

#endif // SERVER_H
