#include "server.h"
#include <thread>
#include <iostream>
#include <boost/bind.hpp>

int main(int argc, char *argv[])
{    
    try
    {
        boost::asio::io_service ios;
        Server server(ios);
        std::thread([&ios]()
        {
            ios.run();
        }).join();
        ios.stop();
    }
    catch (std::exception& e)
    {
        std::cout << "Exception: " << e.what() << "\n";
    }
    return 0;
}
